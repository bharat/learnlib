package learner;

/**
 * 
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.learnlib.api.SUL;
import de.learnlib.api.oracle.SymbolQueryOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.oracle.membership.SULSymbolQueryOracle;
import hads.YannakakisEQOracle;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.words.Word;
import utils.LearningOptions;
import utils.LearningResult;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;

/**
 * @author bharat
 *
 */
public final class Main {

	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, ParseException {
		Options options = new Options();
		Option algOpt = Option.builder().longOpt("algorithm").argName("alg").hasArg(true).hasArg().numberOfArgs(1)
				.desc("Specify the learning alg: ADT, TTT, RS").build();
		Option cttOpt = Option.builder().longOpt("conformance").argName("ctt").hasArg(true).hasArg()
				.desc("Specify the conformance tester: BFS, HADS").build();
		Option modelOpt = Option.builder().longOpt("model_file").argName("model").hasArg(true)
				.desc("Specify the FULL path for of the DOT file.").build();
		Option modelDirOpt = Option.builder().longOpt("model_dir").argName("model_dir").hasArg(true)
				.desc("Specify the FULL path for of the DOT files dir.").build();
		Option repeatOpt = Option.builder().longOpt("repeat").argName("repeat").hasArg(true)
				.desc("Number of times to repeat each experiment.").build();
		Option passOpt = Option.builder().longOpt("pass").argName("pass").hasArg(true).desc("Passthrough mode for L#.")
				.build();
		Option exampleOpt = Option.builder().longOpt("example").argName("example").hasArg(false)
				.desc("Run the example model").build();
		Option cacheOpt = Option.builder().longOpt("cache").argName("cache").hasArg(false)
				.desc("Set if cache is to be around learner and tester both").build();
		options.addOption(cacheOpt);
		options.addOption(repeatOpt);
		options.addOption(algOpt);
		options.addOption(cttOpt);
		options.addOption(modelOpt);
		options.addOption(modelDirOpt);
		options.addOption(passOpt);
		options.addOption(exampleOpt);
//		HelpFormatter formatter = new HelpFormatter();

//		final PrintWriter writer = new PrintWriter(System.out);
//		formatter.printHelp("LearnLib-Experiments", options);
//		writer.flush();
		// CLI parser
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);

		if (cmd.hasOption("pass")) {
			// Pass-through the hypothesis and the stuff to the H-ADS oracle.
			Path path_sul = Paths.get(cmd.getOptionValue("model_file"));
			GraphvizParser p = new GraphvizParser(path_sul);
			CompactMealy<String, String> mm = p.createMachine();
			SUL<String, String> sul = new MealySimulatorSUL<String, String>(mm);
			SymbolQueryOracle<String, String> sulOracle = new SULSymbolQueryOracle<String, String>((sul));

			Path path_hyp = Paths.get(cmd.getOptionValue("pass"));
			GraphvizParser hyp_parse = new GraphvizParser(path_hyp);
			CompactMealy<String, String> hyp = hyp_parse.createMachine();
			YannakakisEQOracle eq = new YannakakisEQOracle(mm.getInputAlphabet(), sulOracle, 1000000000l, 1000000000l);
			DefaultQuery<String, Word<String>> ce = eq.findCounterExample(hyp, hyp.getInputAlphabet());
			String ceStr = ce.toString();
			String outputQuery = ce.toString().substring(ceStr.indexOf("|") + 1, ceStr.indexOf("/"));
			String sulResponse = ceStr.substring(ceStr.indexOf("/") + 1, ceStr.indexOf("]"));
			System.out.println(outputQuery);
			System.out.println(sulResponse);
			// System.out.println(eq.reportInputs());
			// System.out.println(eq.reportResets());
			System.exit(0);
		}

		Path resultFile = Paths.get("/home/bharat/rust/automata-lib/learnlib_results.csv");
		Files.deleteIfExists(resultFile);
		try {
			// Create the empty file with default permissions, etc.
			Files.createFile(resultFile);
		} catch (FileAlreadyExistsException x) {

			System.err.format("file named %s" + " already exists%n", resultFile);
		} catch (IOException x) {
			// Some other sort of failure, such as permissions.
			System.err.format("createFile error: %s%n", x);
		}

		Files.writeString(resultFile,
				"name, learned, rounds, num_states, num_inputs, learn_inputs, learn_resets, test_inputs, test_resets, ads_score,learning_algorithm\n",
				StandardOpenOption.APPEND);
		LearningOptions.TestingMethod ctt = LearningOptions.TestingMethod.valueOf(cmd.getOptionValue("conformance"));
		System.out.println(cmd.getParsedOptionValue("algorithm").toString());
		for (String alg_str : cmd.getOptionValue("algorithm").split(",")) {
			LearningOptions.LearningMethod alg = LearningOptions.LearningMethod.valueOf(alg_str);
			int numRepeat = 1;
			if (cmd.hasOption("repeat")) {
				numRepeat = Integer.valueOf(cmd.getOptionValue("repeat"));
			}

			long input_limit = 1000000000;
			long reset_limit = input_limit;
			if (cmd.hasOption("example")) {
				Path path = Paths.get(cmd.getOptionValue("model_file"));
				SUL<String, String> sul = new ExampleSUL();
				GraphvizParser p = new GraphvizParser(path);
				CompactMealy<String, String> mm = p.createMachine();
				try {
					// runControlledExperiment for detailed statistics, runSimpleExperiment for just
					// the result
					LearningResult result = BasicLearner.runControlledExperiment(sul, alg, ctt, mm.getInputAlphabet(),
							mm, path, input_limit, reset_limit, false);
					result.alg = alg;
					result.ctt = ctt;
					result.input_size = mm.getInputAlphabet().size();
					result.numStates = mm.size();
					String filename = path.getFileName().toString();
					String resultString = String.format("%s, %s\n", filename, result.to_csv_entry());
//					Files.writeString(resultFile, resultString, StandardOpenOption.APPEND);
				} finally {
					if (sul instanceof AutoCloseable) {
						try {
							((AutoCloseable) sul).close();
						} catch (Exception exception) {
							// should not happen
						}
					}
				}
			}

			if (cmd.hasOption("model_file")) {
				Path path = Paths.get(cmd.getOptionValue("model_file"));
				for (int i = 0; i < numRepeat; i++) {
					GraphvizParser p = new GraphvizParser(path);
					CompactMealy<String, String> mm = p.createMachine();
					SUL<String, String> sul = new MealySimulatorSUL<String, String>(mm);

					try {
						// runControlledExperiment for detailed statistics, runSimpleExperiment for just
						// the result
						LearningResult result = BasicLearner.runControlledExperiment(sul, alg, ctt,
								mm.getInputAlphabet(), mm, path, input_limit, reset_limit, cmd.hasOption("cacheOpt"));
						result.alg = alg;
						result.ctt = ctt;
						result.input_size = mm.getInputAlphabet().size();
						result.numStates = mm.size();
						String filename = path.getFileName().toString();
						String resultString = String.format("%s, %s\n", filename, result.to_csv_entry());
						Files.writeString(resultFile, resultString, StandardOpenOption.APPEND);
					} finally {
						if (sul instanceof AutoCloseable) {
							try {
								((AutoCloseable) sul).close();
							} catch (Exception exception) {
								// should not happen
							}
						}
					}
				}
			}
			if (cmd.hasOption("model_dir")) {
				try (Stream<Path> paths = Files.walk(Paths.get(cmd.getOptionValue("model_dir")))) {
					List<Path> files = paths.filter(Files::isRegularFile).filter(x -> !x.toFile().isHidden())
							.collect(Collectors.toList());

					// String model1 =
					// "/home/bharat/rust/automata-lib/automata_wiki/Mealy/principle/BenchmarkTLS/GnuTLS_3.3.12_client_regular.dot";
					for (Path path : files) {
						GraphvizParser p = new GraphvizParser(path);
						CompactMealy<String, String> mm = p.createMachine();
						for (int i = 0; i < numRepeat; i++) {

							SUL<String, String> sul = new MealySimulatorSUL<String, String>(mm);
							try {
								// runControlledExperiment for detailed statistics, runSimpleExperiment for just
								// the result
								LearningResult result = BasicLearner.runControlledExperiment(sul, alg, ctt,
										mm.getInputAlphabet(), mm, path, input_limit, reset_limit,
										cmd.hasOption("cacheOpt"));
								result.alg = alg;
								result.ctt = ctt;
								result.input_size = mm.getInputAlphabet().size();
								result.numStates = mm.size();
								String filename = path.getFileName().toString();
								String resultString = String.format("%s, %s\n", filename, result.to_csv_entry());
								Files.writeString(resultFile, resultString, StandardOpenOption.APPEND);
							} finally {
								if (sul instanceof AutoCloseable) {
									try {
										((AutoCloseable) sul).close();
									} catch (Exception exception) {
										// should not happen
									}
								}
							}
						}
					}
				}
			}

		}
		System.out.println("Finished!");
	}
}
