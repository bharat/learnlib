package learner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;
import java.util.Collection;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.util.automata.Automata;
import net.automatalib.visualization.dot.DOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import de.learnlib.filter.cache.mealy.SymbolQueryCache;
import com.google.common.collect.Lists;

import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.adt.config.ADTExtenders;
import de.learnlib.algorithms.adt.config.LeafSplitters;
import de.learnlib.algorithms.adt.config.SubtreeReplacers;
import de.learnlib.algorithms.adt.learner.ADTLearner;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.SUL;
import de.learnlib.api.algorithm.LearningAlgorithm;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle.MealyMembershipOracle;
import de.learnlib.api.oracle.SymbolQueryOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.filter.statistic.Counter;
import de.learnlib.filter.statistic.sul.ResetCounterSUL;
import de.learnlib.filter.statistic.sul.SymbolCounterSUL;
import de.learnlib.oracle.equivalence.MealySimulatorEQOracle;
import de.learnlib.algorithms.lstar.ce.ObservationTableCEXHandlers;
import de.learnlib.algorithms.lstar.closing.ClosingStrategies;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealy;
import de.learnlib.algorithms.rivestschapire.RivestSchapireMealy;
import de.learnlib.oracle.membership.SULSymbolQueryOracle;
import de.learnlib.util.Experiment.MealyExperiment;
import net.automatalib.words.impl.GrowingMapAlphabet;
import utils.LearningOptions.LearningMethod;
import utils.LearningOptions.TestingMethod;
import hads.YannakakisEQOracle;
import utils.LearningResult;

/**
 * General learning testing framework. All basic settings are at the top of this
 * file and can be configured by hard-coding or by simply changing them from
 * your own code. Method "runSimpleExperiment" learns a model and writes it to a
 * file. Method "runControlledExperiment" shows extra statistics and
 * intermediate hypotheses, which you can customize.
 * 
 * Based on the learner experiment setup of Joshua Moerman,
 * https://gitlab.science.ru.nl/moerman/Learnlib-Experiments
 * 
 * @author Ramon Janssen
 */
public class BasicLearner {
	// ***********************************************************************************//
	// Learning settings (hardcoded, simply set to a different value to change
	// learning) //
	// ***********************************************************************************//
	/**
	 * name to give to the resulting .dot-file and .pdf-file (extensions are added
	 * automatically)
	 */
	public static String // extension .pdf is added automatically
	FINAL_MODEL_FILENAME = "learnedModel", INTERMEDIATE_HYPOTHESIS_FILENAME = "hypothesis"; // a number gets appended
																							// for every iteration
	/**
	 * For controlled experiments only: store every hypotheses as a file. Useful for
	 * 'debugging' if the learner does not terminate (hint: the TTT-algorithm
	 * produces many hypotheses).
	 */
	public static boolean saveAllHypotheses = false;
	/**
	 * MaxDepth-parameter for W-method and Wp-method. This acts as the parameter 'n'
	 * for an n-complete test suite. Typically not larger than 3. Decrease for
	 * quicker runs.
	 */
	public static int w_wp_methods_maxDepth = 3;

	public static LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> loadLearner(
			LearningMethod learningMethod, SymbolQueryOracle<String, String> sulOracle, Alphabet<String> alphabet) {
		switch (learningMethod) {
		case LStar:
			return new ExtensibleLStarMealy<String, String>(alphabet, sulOracle, Lists.<Word<String>>newArrayList(),
					ObservationTableCEXHandlers.CLASSIC_LSTAR, ClosingStrategies.CLOSE_SHORTEST);
		case RS:
			return new RivestSchapireMealy<String, String>(alphabet, sulOracle, Lists.<Word<String>>newArrayList(),
					ClosingStrategies.CLOSE_SHORTEST);
//		case RS:
//			return new ExtensibleLStarMealy<String, String>(alphabet, sulOracle, Lists.<Word<String>>newArrayList(),
//					ObservationTableCEXHandlers.RIVEST_SCHAPIRE, ClosingStrategies.CLOSE_SHORTEST);
		case TTT:
			return new TTTLearnerMealy<String, String>(alphabet, sulOracle, AcexAnalyzers.BINARY_SEARCH_FWD);
		case KearnsVazirani:
			return new KearnsVaziraniMealy<String, String>(alphabet, sulOracle, false, AcexAnalyzers.LINEAR_FWD);
		case ADT:
			return new ADTLearner<String, String>(alphabet, sulOracle, LeafSplitters.DEFAULT_SPLITTER,
					ADTExtenders.EXTEND_BEST_EFFORT, SubtreeReplacers.EXHAUSTIVE_BEST_EFFORT);
		default:
			throw new RuntimeException("No learner selected");
		}
	}

	public static EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> loadTester(
			TestingMethod testMethod, SUL<String, String> sul, MealyMembershipOracle<String, String> sulOracle,
			CompactMealy<String, String> mm, long input_limit, long reset_limit) {
		switch (testMethod) {
		case HADS:
			return new YannakakisEQOracle(mm.getInputAlphabet(), sulOracle, input_limit, reset_limit);
		case BFS:
			return new MealySimulatorEQOracle<String, String>(mm);
		default:
			throw new RuntimeException("No test oracle selected!");
		}
	}

	// **********************************************//
	// Methods to start the actual learning process //
	// **********************************************//
	/**
	 * More detailed example of running a learning experiment. Starts learning, and
	 * then loops testing, and if counterexamples are found, refining again. Also
	 * prints some statistics about the experiment
	 * 
	 * @param learner   learner Learning algorithm, wrapping the SUL
	 * @param eqOracle  Testing algorithm, wrapping the SUL
	 * @param nrSymbols A counter for the number of symbols that have been sent to
	 *                  the SUL (for statistics)
	 * @param nrResets  A counter for the number of resets that have been sent to
	 *                  the SUL (for statistics)
	 * @param alphabet  Input alphabet
	 * @param path
	 * @throws Exception
	 */
	public static LearningResult runControlledExperiment(
			LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle, Counter nrSymbols,
			Counter nrResets, Counter learningNrSymbols, Counter learningNrResets, Alphabet<String> alphabet, Path path,
			CompactMealy<String, String> ref_machine, boolean cache_full) throws Exception {
		try {
			// prepare some counters for printing statistics
			int stage = 1;
			long lastNrResetsValue = 0, lastNrSymbolsValue = 0;
			long learninglastNrResetsValue = 0, learninglastNrSymbolsValue = 0;

			// learn the first hypothesis
			learner.startLearning();
			long learn_inputs = 0;
			long learn_resets = 0;
			long test_inputs = 0;
			long test_resets = 0;
			while (true) {
				// store hypothesis as file
				if (saveAllHypotheses) {
					String outputFilename = INTERMEDIATE_HYPOTHESIS_FILENAME + stage;
					produceOutput(outputFilename, learner.getHypothesisModel(), alphabet, false);
					System.out.println("model size " + learner.getHypothesisModel().getStates().size());
				}

				// Print statistics
				System.out.println(stage + ": " + Calendar.getInstance().getTime());
				// Log number of queries/symbols
				System.out.println("Hypothesis size: " + learner.getHypothesisModel().size() + " states");
				long roundResets = learningNrResets.getCount() - learninglastNrResetsValue,
						roundSymbols = learningNrSymbols.getCount() - learninglastNrSymbolsValue;
				learn_inputs += roundSymbols;
				learn_resets += roundResets;
				System.out.println(
						"learning queries/symbols: " + learningNrResets.getCount() + "/" + learningNrSymbols.getCount()
								+ "(" + roundResets + "/" + roundSymbols + " this learning round)");
				learninglastNrResetsValue = learningNrResets.getCount();
				learninglastNrSymbolsValue = learningNrSymbols.getCount();
//				System.out.println("After learning, before checking we learned");
//				System.out.println("nrResets = " + nrResets.getCount());
//				System.out.println("nrSymbols = " + nrSymbols.getCount());
				// Search for CE
				boolean learned = Automata.findShortestSeparatingWord(ref_machine, learner.getHypothesisModel(),
						ref_machine.getInputAlphabet()) == null;
				DefaultQuery<String, Word<String>> ce = null;

				if (learned) {
					LearningResult result = new LearningResult(learned, stage, learn_inputs, learn_resets, test_inputs,
							test_resets);
					return result;

				} else {
					System.out.println("Searching for CE:");
//					System.out.println("Before finding CE using H-ADS.");
//					System.out.println("nrResets = " + nrResets.getCount());
//					System.out.println("nrSymbols = " + nrSymbols.getCount());
					ce = eqOracle.findCounterExample(learner.getHypothesisModel(), alphabet);
				}
//				System.out.println("After testing for CE.");
//				System.out.println("nrResets = " + nrResets.getCount());
//				System.out.println("nrSymbols = " + nrSymbols.getCount());
				// Log number of queries/symbols
				if (cache_full) {
					roundResets = learningNrResets.getCount() - learninglastNrResetsValue;
					roundSymbols = learningNrSymbols.getCount() - learninglastNrSymbolsValue;
					learninglastNrResetsValue = learningNrResets.getCount();
					learninglastNrSymbolsValue = learningNrSymbols.getCount();
				} else {
					roundResets = nrResets.getCount() - lastNrResetsValue;
					roundSymbols = nrSymbols.getCount()-lastNrSymbolsValue;
					lastNrResetsValue = nrResets.getCount();
					lastNrSymbolsValue = nrSymbols.getCount();
				}

				test_inputs += roundSymbols;
				test_resets += roundResets;

//				System.out.println("Last Reset vals= " + lastNrResetsValue);
//				System.out.println("Last Input vals= " + lastNrSymbolsValue);

				System.out.println("testing queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount() + "("
						+ roundResets + "/" + roundSymbols + " this testing round)");

//				lastNrResetsValue = nrResets.getCount();
//				lastNrSymbolsValue = nrSymbols.getCount();

				if (ce == null) {
					// No counterexample found, stop learning
					System.out.println("\nFinished learning!");

					produceOutput(FINAL_MODEL_FILENAME, learner.getHypothesisModel(), alphabet, true);
					Files.writeString(Paths.get("/home/bharat/rust/automata-lib/ttt_results.csv"),
							String.format("%s,%b,%d,%d,%d,%d,%d\n", path.getFileName(), learned, stage,
									learn_inputs, learn_resets, test_inputs, test_resets),
							StandardOpenOption.APPEND);
					LearningResult result = new LearningResult(learned, stage, learn_inputs, learn_resets, test_inputs,
							test_resets);
					return result;

				} else {
					// Counterexample found, rinse and repeat
					System.out.println();
					stage++;
					learner.refineHypothesis(ce);
				}
			}
		} catch (Exception e) {
			String errorHypName = "hyp.before.crash.dot";
			produceOutput(errorHypName, learner.getHypothesisModel(), alphabet, true);
			throw e;
		}

	}

	/**
	 * More detailed example of running a learning experiment. Starts learning, and
	 * then loops testing, and if counterexamples are found, refining again. Also
	 * prints some statistics about the experiment
	 * 
	 * @param sul            Direct access to SUL
	 * @param learningMethod One of the default learning methods from this class
	 * @param testingMethod  One of the default testing methods from this class
	 * @param alphabet       Input alphabet
	 * @param alphabet       Input alphabet
	 * @param path
	 * @throws IOException
	 */
	public static LearningResult runControlledExperiment(SUL<String, String> sul, LearningMethod learningMethod,
			TestingMethod testingMethod, Collection<String> alphabet, CompactMealy<String, String> mm, Path path,
			long input_limit, long reset_limit, boolean cache_full) throws IOException {
		Alphabet<String> learnlibAlphabet = new GrowingMapAlphabet<String>(alphabet);
		LearningSetup learningSetup = new LearningSetup(sul, learningMethod, testingMethod, learnlibAlphabet, mm,
				input_limit, reset_limit, cache_full);
		try {
			return runControlledExperiment(learningSetup.learner, learningSetup.eqOracle, learningSetup.nrSymbols,
					learningSetup.nrResets, learningSetup.learningNrSymbols, learningSetup.learningNrResets,
					learnlibAlphabet, path, mm, cache_full);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// ************************//
	// Some auxiliary methods //
	// ************************//
	/**
	 * Produces a dot-file
	 * 
	 * @param fileName filename without extension
	 * @param model
	 * @param alphabet
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void produceOutput(String fileName, MealyMachine<?, String, ?, String> model,
			Alphabet<String> alphabet, boolean verboseError) throws FileNotFoundException, IOException {
		PrintWriter dotWriter = new PrintWriter(fileName + ".dot");
		GraphDOT.write(model, alphabet, dotWriter);
		dotWriter.close();
	}

	/**
	 * Helper class to configure a learning and equivalence oracle. Tell it which
	 * learning and testing method you want, and it produces the corresponding
	 * oracles (and counters for statistics) as attributes.
	 */
	public static class LearningSetup {
		public final EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle;
		public final LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner;
		public final Counter nrSymbols, nrResets;
		public final Counter learningNrSymbols, learningNrResets;

		public LearningSetup(SUL<String, String> learningBaseSUL, LearningMethod learningMethod,
				TestingMethod testingMethod, Alphabet<String> alphabet, CompactMealy<String, String> mm,
				long input_limit, long reset_limit, boolean cache_full) {
			MealySimulatorSUL<String, String> sul = new MealySimulatorSUL<>(mm);
			// Wrap the SUL in counters for symbols/resets, so that we can record some
			// statistics

			// we should use the sul only through those wrappers
//			sul = resetCounterSul;
			// Most testing/learning-algorithms want a membership-oracle instead of a SUL
			// directly

			SymbolCounterSUL<String, String> learningSymbolCounterSul = new SymbolCounterSUL<String, String>(
					"learning symbol counter", learningBaseSUL);
			ResetCounterSUL<String, String> learningResetCounterSul = new ResetCounterSUL<String, String>(
					"learning reset counter", learningSymbolCounterSul);
			SymbolQueryOracle<String, String> learningSULOracle = new SULSymbolQueryOracle<String, String>(
					(learningResetCounterSul));
			learningNrSymbols = learningSymbolCounterSul.getStatisticalData();
			learningNrResets = learningResetCounterSul.getStatisticalData();
			SymbolQueryCache<String, String> cachedSULOracle = new SymbolQueryCache<>(learningSULOracle, alphabet);

			if (cache_full) {
				// If the cache is around both, use the same sul.
				learner = loadLearner(learningMethod, cachedSULOracle, alphabet);
				eqOracle = loadTester(testingMethod, null, cachedSULOracle, mm, input_limit, reset_limit);
				nrSymbols = learningNrSymbols;
				nrResets = learningNrResets;
			} else {
				// If the cache is not wrapped around both, we have to make a new oracle for the
				// tester.
				SymbolCounterSUL<String, String> symbolCounterSul = new SymbolCounterSUL<String, String>(
						"symbol counter", sul);
				ResetCounterSUL<String, String> resetCounterSul = new ResetCounterSUL<String, String>("reset counter",
						symbolCounterSul);
				nrSymbols = symbolCounterSul.getStatisticalData();
				nrResets = resetCounterSul.getStatisticalData();
				SymbolQueryOracle<String, String> testingOracle = new SULSymbolQueryOracle<String, String>(
						resetCounterSul);
				learner = loadLearner(learningMethod, cachedSULOracle, alphabet);
				eqOracle = loadTester(testingMethod, null, testingOracle, mm, input_limit, reset_limit);
			}
			// Choosing a learner
//			learner = loadLearner(learningMethod, sulOracle, alphabet);

			// Choosing an equivalence oracle

		}
	}
}
