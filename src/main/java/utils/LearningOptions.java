package utils;

public final class LearningOptions {
	// ********************************************//
	// Predefined learning and testing algorithms //
	// ********************************************//
	/**
	 * The learning algorithms. LStar is the basic algorithm, TTT performs much
	 * faster but is a bit more inaccurate and produces more intermediate
	 * hypotheses, so test well.
	 */
	public enum LearningMethod {
		LStar, RS, TTT, KearnsVazirani, ADT
	}

	/**
	 * The testing algorithms. Random walk is the simplest, but may perform badly on
	 * large models: the chance of hitting a hard-to-reach transition is very small.
	 * WMethod and WpMethod are smarter. With UserQueries, the user acts as
	 * equivalence oracle: have a look at the hypothesis, and try to think of one.
	 */
	public enum TestingMethod {
		BFS, HADS
	}
}
