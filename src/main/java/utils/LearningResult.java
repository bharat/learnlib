/**
 * 
 */
package utils;

import utils.LearningOptions.LearningMethod;
import utils.LearningOptions.TestingMethod;

/**
 * @author bharat
 *
 */
public class LearningResult {
	public long test_resets = 0;
	public long test_inputs = 0;
	public long learn_inputs = 0;
	public long learn_resets = 0;
	public int rounds = 0;
	public int input_size = 0;
	public boolean learned = true;
	public int numStates = 0;
	public LearningOptions.LearningMethod alg = null;
	public LearningOptions.TestingMethod ctt = null;

	public LearningResult(boolean learned, int stage, long learn_inputs, long learn_resets, long test_inputs,
			long test_resets) {
		this.learned = learned;
		this.learn_inputs = learn_inputs;
		this.learn_resets = learn_resets;
		this.test_inputs = test_inputs;
		this.test_resets = test_resets;
		this.rounds = stage;
		return;
	}

	public String to_csv_entry() {
		return String.format("%s, %d, %d, %d, %d, %d, %d, %d,0, %s", this.learned, rounds, numStates, input_size, learn_inputs,
				learn_resets, test_inputs, test_resets, alg);
	}
}
